package com.sound.dao;

import com.sound.model.UserTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/5/7.
 */
@Repository
public interface UserTypeDAO extends JpaRepository<UserTypeModel,Long> {
}
