package com.sound.dao;

import com.sound.model.UserModel;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/4/28.
 */
@Repository
public interface UserDAO extends JpaRepository<UserModel, Long> {

	@Query("select u from userinfo u where u.userId=:userId and u.password=:password")
	@Modifying
	public UserModel login(@Param("userId") String userId, @Param("password") String password);

	@Query("select u from userinfo u where u.userId=:userId")
	public UserModel getByUserName(@Param("userId") String userId);

	@Query("select u from userinfo u where u.nowDeviceId=:nowDeviceId")
	public Iterable<UserModel> findByDeviceId(@Param("nowDeviceId") String nowDeviceId);

}
