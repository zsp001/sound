package com.sound.dao;

import com.sound.model.Device;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/5/3.
 */
@Repository
public interface DeviceDAO extends JpaRepository<Device,Long> {
	
	@Query("select u from device u where u.userId=:userId")
	public Iterable<Device> findByUserId(@Param("userId") String userId);
	
	@Query("select u from device u where u.deviceId=:deviceId")
	public Device findDeviceById(@Param("deviceId") String deviceId);
}
