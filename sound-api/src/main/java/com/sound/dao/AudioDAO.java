
package com.sound.dao;

import com.sound.model.AudioModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AudioDAO extends JpaRepository<AudioModel,Long>{
	
	@Query("select u from audio u where u.albumId=:albumId")
	@Modifying
	public List<AudioModel> findByAlbumId(@Param("albumId") Long albumId);

	@Query("select u from audio u")
	public Page<AudioModel> findByPage(Pageable pageable);

	@Query("select u from audio u where u.title like %?1% or u.authorName like %?1%")
	public Page<AudioModel> search(String search, Pageable pageable);
}

