package com.sound.dao;

import com.sound.model.BigTypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/5/8.
 */
@Repository
public interface BigTypeDAO extends JpaRepository<BigTypeModel,Long> {
}
