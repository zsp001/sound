package com.sound.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sound.model.CollectModel;

@Repository
public interface CollectDAO extends JpaRepository<CollectModel,Long> {

}
