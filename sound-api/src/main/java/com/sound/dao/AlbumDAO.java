package com.sound.dao;

import com.sound.model.AlbumModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumDAO extends JpaRepository<AlbumModel, Long>{

    @Query("select u from album u")
    public Page<AlbumModel> findByPage(Pageable pageable);
    
    @Query("select u from album u where u.display=true")
    public Iterable<AlbumModel> findAlbumByDispay();

    @Query("select u from album u where u.title like %?1% or u.authorName like %?1%")
    public Page<AlbumModel> search(String search,Pageable pageable);
}
