
package com.sound.dao;
import com.sound.model.TypeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/5/8.
 */

@Repository
public interface TypeDAO extends JpaRepository<TypeModel,Long>{

}
