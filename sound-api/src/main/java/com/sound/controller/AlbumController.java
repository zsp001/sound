package com.sound.controller;


import com.sound.common.ParamCode;
import com.sound.common.Response;
import com.sound.model.AlbumModel;
import com.sound.model.AudioModel;
import com.sound.model.TypeModel;
import com.sound.service.AlbumService;
import com.sound.service.AudioService;
import com.sound.service.RedisService;
import com.sound.service.TypeService;
import com.sound.vo.AlbumVo;
import com.sound.vo.TypeAlbumVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 专辑控制类
 * @author Administrator
 *
 */
@RestController
@Api(tags="专辑")
@RequestMapping(path="/album")
public class AlbumController extends BaseController<AlbumModel>{

	private static final Logger logger = LoggerFactory.getLogger(AlbumController.class);

	@Autowired
	private AudioService audioService;
	
	@Autowired
	private AlbumService albumService;

	@Autowired
	private TypeService typeService;

	@Autowired
	private RedisService redisService;

	@GetMapping(path="/search")
	@ApiOperation(value = "搜索查找",tags = "")
	public Response search(String search,int page,int size){
		Response resp = new Response(true);
		try{
			resp.setRespone(ParamCode.SUCSESS);
			Map<String,Object> resultMap  =new HashMap<>();
			Sort sort = Sort.by(Sort.Direction.DESC, "playCount");

			Page<AlbumModel> pageAlbum = albumService.search(search,PageRequest.of(page, size,sort));
			Sort sort1 = Sort.by(Sort.Direction.DESC, "readCount");

			Page<AudioModel> page1 = audioService.search(search,PageRequest.of(page, size,sort1));
			List<AlbumModel> albumList = new ArrayList<>();
			List<AudioModel> audioList = new ArrayList<>();

			Map<Long,Object> albumMap = new HashMap<>();
			Map<Long,Object> audioMap = new HashMap<>();

			List<AlbumModel> albumP = pageAlbum.getContent();
			int m = 0;
			for(AlbumModel album:albumP){
				if(albumMap.containsKey(album.getId())){
					continue;
				}else{
					albumMap.put(album.getId(),"");
					albumList.add(album);
					m++;
				}
			}
			List<AudioModel> audioP = page1.getContent();
			for(AudioModel audioModel:audioP){
				if(m>10 || audioMap.containsKey(audioModel.getId())
						|| albumMap.containsKey(audioModel.getAlbumId())){
					continue;

				}else{
					audioList.add(audioModel);
					m++;
				}
			}
			resultMap.put("albumList",albumList);
			resultMap.put("audioList",audioList);
			resp.setData(resultMap);
		}catch (Exception e){
			logger.error("album search fail:",e);
			resp.setRespone(ParamCode.FAIL);
		}

		return resp;
	}
	/**
	 * 查看专辑详情
	 */
	@GetMapping(path="/findById.do")
	@ApiOperation(value = "查看专辑详情",tags = "")
	public Response findById(Long id) {
		Response<AlbumVo> resp = new Response<>(true);
		try{
			AlbumModel model = albumService.findById(id);
			if(model==null) {
				resp.setRespone(ParamCode.FAIL);
				return resp;
			}

			TypeModel model1 = typeService.findById(model.getTypeId());

			AlbumVo vo = new AlbumVo();
			vo.setBigTypeId(model1.getBigTypeId());
			vo.setCreateTime(model.getCreateTime());
			vo.setId(model.getId());
			vo.setImgMaxUrl(model.getImgMaxUrl());
			vo.setImgMinUrl(model.getImgMinUrl());
			vo.setTicket(model.getTicket());
			vo.setTitle(model.getTitle());
			vo.setType(model.getType());
			vo.setTag(model.getTag());
			vo.setPlayCount(model.getPlayCount());
			vo.setAudioCount(model.getAudioCount());
			vo.setSummary(model.getSummary());
			List<AudioModel> au = audioService.findByAlbumId(id);
			vo.setAlbumList(au);
			resp.setData(vo);
			resp.setRespone(ParamCode.SUCSESS);
		}catch (Exception e){
			logger.error("findById album fail:",e);
			resp.setRespone(ParamCode.FAIL);
			return resp;
		}

		return resp;
	}
	
	@GetMapping(path="/add/playCount.do")
	@ApiOperation(value = "专辑播放数+1",tags = "")
	public Response<Void> play( @RequestParam("id") long id){
		Response<Void> resp = new Response<Void>(true);
		try {
			AlbumModel albumModel =  super.service.findById(id);
			long playCount = albumModel.getPlayCount() + 1;
			albumModel.setPlayCount(playCount);
			super.service.save(albumModel);
		}catch(Exception e) {
			resp = new Response<Void>(false);
		}
		return resp;
		
	}
	@GetMapping(path="/findByExamplePage.do")
	public Response<Page<AlbumModel>> findByPage(AlbumModel t, @RequestParam("size")int size){
		return super.findByPage(t, 0, size);
	}

	/**
	 * 分页查找专辑
	 * @param page
	 * @return
	 */
	@GetMapping(path="/findByPage.do")
	@ApiOperation(value = "分页查找专辑",tags = "")
	public Response findByPage(int page) {
		Response resp = new Response();
		try{
			Pageable pageable = new PageRequest(page, 10, new Sort(Sort.Direction.DESC,"createTime"));
			Page<AlbumModel> list = albumService.findByPage(pageable);
			resp.setRespone(ParamCode.SUCSESS);
		}catch (Exception e){
			logger.error("findByPage album fail:",e);
			resp.setRespone(ParamCode.FAIL);
			return resp;
		}

		return resp;

	}

	@GetMapping(path = "/findAlbum")
	@ApiOperation(value = "获取单个type下所有专辑",tags = "",response = AlbumModel.class)
	public Response<Map<String,Object>> findAlbum(Long typeId){

		Response<Map<String,Object>> resp = new Response<>(true);
		AlbumModel model = new AlbumModel();
		model.setTypeId(typeId);
		try{
			Iterable<AlbumModel> iterable =albumService.findAll(Example.of(model));
			Map<String,Object> map = new HashMap<>();
			map.put("result",iterable);
			map.put("imghost",imgsDsownload);
			map.put("audiohost",soundDownload);
			resp.setRespone(ParamCode.SUCSESS);
			resp.setData(map);
			return resp;
		}catch (Exception  e){
			logger.error("findAlbum fail:",e);
			resp.setRespone(ParamCode.FAIL);
		}
		
		return resp;

	}

	@GetMapping(path = "/batchFindAlbum")
	@ApiOperation(value = "批量获取type下所有专辑",tags = "",response = TypeAlbumVo.class)
	public Response<Map<String,Object>> batchFindAlbum(String typeIds){
		Response  resp = new Response();
		List<TypeAlbumVo> result = new ArrayList<>();
		List<Long> lsit = new ArrayList<>();
		String[] idList = typeIds.split(",");
		for(int i= 0;i<idList.length;i++){
			lsit.add(Long.valueOf(idList[i]));
		}
		try{
			Iterable<TypeModel> typeModels =typeService.findAllById(lsit);
			Iterator<TypeModel> ite =typeModels.iterator();
			while(ite.hasNext()){
				TypeAlbumVo vo = new TypeAlbumVo();
				TypeModel tymodel = ite.next();
				AlbumModel model = new AlbumModel();
				model.setTypeId(tymodel.getId());
				Iterable<AlbumModel> iterable =albumService.findAll(Example.of(model));
				vo.setAlbumList(iterable);
				vo.setType(tymodel);
				result.add(vo);
			}
			Map<String,Object> map = new HashMap<>();
			map.put("result",result);
			map.put("imghost",imgsDsownload);
			map.put("audiohost",soundDownload);

			resp.setRespone(ParamCode.SUCSESS);
			resp.setData(map);
			return resp;
		}catch (Exception e){
			logger.error("batchFindAlbum fail:",e);
		}
		resp.setRespone(ParamCode.FAIL);
		return resp;
	}
	
	
	@GetMapping(path = "/findAlbumByDisplay")
	@ResponseBody
	@ApiOperation(value = "获取单个type下所有专辑",tags = "",response = AlbumModel.class)
	public Response<Iterable<AlbumModel>> findAlbumByDispay(){
		Response<Iterable<AlbumModel>> resp=new Response<Iterable<AlbumModel>>(true);
		Iterable<AlbumModel> iterable =albumService.findAlbumByDispay();
		resp.setData(iterable);
		return resp;
	}
}
