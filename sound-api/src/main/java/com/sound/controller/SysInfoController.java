package com.sound.controller;

import ch.qos.logback.core.util.SystemInfo;
import com.alibaba.fastjson.JSON;
import com.sound.common.ParamCode;
import com.sound.common.Response;
import com.sound.model.SystemMsgModel;
import com.sound.model.UserModel;
import com.sound.model.UserMsgModel;
import com.sound.service.RedisService;
import com.sound.service.SysMsgService;
import com.sound.service.UserMsgService;
import com.sound.vo.SystemMsgVo;
import com.sound.vo.UserSysMsgVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by Administrator on 2018/4/29.
 */

/**
 * 系统消息
 */
@RestController
@RequestMapping(path="sysInfo")
@Api(tags="系统消息")
public class SysInfoController extends BaseController<SystemMsgModel>{

    private static final Logger logger = LoggerFactory.getLogger(SysInfoController.class);

    @Autowired
    private RedisService redisService;

    @Autowired
    private SysMsgService sysMsgService;

    @Autowired
    private UserMsgService userMsgService;
    
    /**
     * 用户系统消息列表
     * @return
     */
    @GetMapping(path="/syslist.do")
    @ApiOperation(value = "用户系统消息列表",tags = "")
    public Response<Map<String,Object>> getList(HttpServletRequest request){

        UserModel userModel = redisService.getUserInfo(request);
        Response<Map<String,Object>> resp = new Response<>(true);
        Map<String,Object> map = new HashMap<>();
        if(userModel==null){
            resp.setRespone(ParamCode.NOLOGIN);
            resp.setData(map);
            return resp;
        }
        try{
            Iterable<SystemMsgModel> list = sysMsgService.findAllByTime();//系统消息

            List<UserMsgModel> userList = userMsgService.findAllByTime(userModel.getUserId());//用户读取消息数

            Map<Long,String> sysMap = new HashMap<>();
            for(UserMsgModel model:userList){
                sysMap.put(model.getSysmsgId(),"");
            }
            List<SystemMsgVo> msgList = new ArrayList<>();
            Iterator<SystemMsgModel> ites = list.iterator();
            int hasRead =0;
            while(ites.hasNext()){
                SystemMsgModel smsg = ites.next();
                SystemMsgVo vo = new SystemMsgVo();
                vo.setImgName(smsg.getImgName());
                vo.setContent(smsg.getContent());
                vo.setCreateTime(smsg.getCreateTime());
                vo.setId(smsg.getId());
                vo.setImgurl(smsg.getImgurl());
                vo.setTitle(smsg.getTitle());
                if(!sysMap.containsKey(smsg.getId())){
                    vo.setIsread(false);
                }else{
                    vo.setIsread(true);
                    hasRead++;
                }
                msgList.add(vo);
            }
            UserSysMsgVo  result = new UserSysMsgVo();
            result.setHasReadNum(hasRead);
            result.setMsgList(msgList);
            result.setNoReadNum(msgList.size()-hasRead);
            map.put("result",result);
            map.put("imghost",imgsDsownload);
            map.put("audiohost",soundDownload);
            resp.setRespone(ParamCode.SUCSESS);
            resp.setData(map);
            return resp;
        }catch (Exception e){
            logger.error("get sysInfo fail:",e);
        }
        resp.setRespone(ParamCode.FAIL);
        return resp;


    }

    /**
     * 修改消息状态
     * @return
     */
    @GetMapping(path="/setStatus")
    @ApiOperation(value = "用户阅读系统消息",tags = "")
    public Response setStatus(HttpServletRequest request,Long sysMsgId){
        Response resp = new Response();
        UserModel userModel = redisService.getUserInfo(request);
        if(userModel==null){
            return new Response(true);
        }
        UserMsgModel msg =new UserMsgModel();
        msg.setStatus(1);
        msg.setUserId(userModel.getUserId());
        msg.setSysmsgId(sysMsgId);
        UserMsgModel msgmodel = userMsgService.save(msg);
        if(msgmodel==null||msgmodel.getStatus()==1){
            resp.setRespone(ParamCode.SUCSESS);
            resp.setData(true);

        }else{
            resp.setRespone(ParamCode.FAIL);

        }
        return resp;
    }



}
