package com.sound.controller;

import com.sound.common.ParamCode;
import com.sound.common.Response;
import com.sound.model.BigTypeModel;
import com.sound.model.TypeModel;
import com.sound.service.BigTypeService;
import com.sound.vo.TypeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Created by Administrator on 2018/5/8.
 */
@RestController
@Api(tags = "类型")
@RequestMapping(path ="/type")
public class TypeController extends BaseController<TypeModel>{

    private static final Logger logger = LoggerFactory.getLogger(TypeController.class);

    @Autowired
    private BigTypeService bigTypeService;


    @GetMapping(path = "findAll")
    @ApiOperation(value = "获取bigTye下所有类型",tags = "",response = TypeVo.class)
    public Response<Map<String,Object>> findAlls() {
        Map<String,Object> map = new HashMap<>();
        Response<Map<String,Object>> resp = new Response<>();
        Iterable<BigTypeModel> bigTypeModels = null;
        Iterable<TypeModel> typeModels = null;
        try{
            bigTypeModels = bigTypeService.findAll();
            typeModels = super.service.findAll();
        }catch (Exception e){
            logger.error("get type fail:",e);
            resp.setRespone(ParamCode.FAIL);
            return resp;
        }
        Iterator<BigTypeModel> its = bigTypeModels.iterator();
        Iterator<TypeModel> typ =typeModels.iterator();

        List<TypeModel> list = new ArrayList<TypeModel>();
        Map<Long,TypeVo> listMap = new HashMap<>();

        while(its.hasNext()){
            BigTypeModel itsModel = its.next();
            TypeVo typeModel = new TypeVo();
            typeModel.setBigTypeModel(itsModel);
            listMap.put(itsModel.getId(),typeModel);
        }

        while(typ.hasNext()){
            TypeModel itss=  typ.next();
            if(listMap.containsKey(itss.getBigTypeId())){
                Long tmp = itss.getBigTypeId();
                TypeVo vo = listMap.get(itss.getBigTypeId());
                if(vo.getTypeModels()==null){
                    List<TypeModel> typeModelList = new ArrayList<>();
                    typeModelList.add(itss);
                    vo.setTypeModels(typeModelList);
                }else {
                    List<TypeModel> typeModelList = vo.getTypeModels();
                    typeModelList.add(itss);
                }
            }
        }
        Collection<TypeVo> collection= listMap.values();
        List<TypeVo> voList = new ArrayList<>(collection);
        map.put("result",voList);
        map.put("imghost",imgsDsownload);
        map.put("audiohost",soundDownload);

        resp.setData(map);
        resp.setRespone(ParamCode.SUCSESS);
        return resp;
    }


}
