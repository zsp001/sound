package com.sound.vo;

import com.sound.model.BigTypeModel;
import com.sound.model.TypeModel;

import java.util.List;

/**
 * Created by Administrator on 2018/5/8.
 */
public class TypeVo {

    private List<TypeModel> typeModels;


    private BigTypeModel bigTypeModel;

    public List<TypeModel> getTypeModels() {
        return typeModels;
    }

    public void setTypeModels(List<TypeModel> typeModels) {
        this.typeModels = typeModels;
    }

    public BigTypeModel getBigTypeModel() {
        return bigTypeModel;
    }

    public void setBigTypeModel(BigTypeModel bigTypeModel) {
        this.bigTypeModel = bigTypeModel;
    }
}
