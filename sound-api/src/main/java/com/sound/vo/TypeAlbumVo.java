package com.sound.vo;

import com.sound.model.AlbumModel;
import com.sound.model.TypeModel;

/**
 * Created by Administrator on 2018/5/9.
 */
public class TypeAlbumVo {

    private TypeModel type;

    private Iterable<AlbumModel> albumList;

    public TypeModel getType() {
        return type;
    }

    public void setType(TypeModel type) {
        this.type = type;
    }

    public Iterable<AlbumModel> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(Iterable<AlbumModel> albumList) {
        this.albumList = albumList;
    }
}
