package com.sound.service;


import com.sound.dao.AlbumDAO;
import com.sound.model.AlbumModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class AlbumService extends BaseService<AlbumModel>{

    @Autowired
    private AlbumDAO albumDAO;

    public Page<AlbumModel> findByPage(Pageable pageable){
        return albumDAO.findByPage(pageable);
    }
    
    public void play( long id){
		AlbumModel albumModel =  this.findById(id);
		long playCount = albumModel.getPlayCount() + 1;
		albumModel.setPlayCount(playCount);
		this.save(albumModel);
	}
    
    public Iterable<AlbumModel> findAlbumByDispay(){
        return albumDAO.findAlbumByDispay();
    }

    public Page<AlbumModel> search(String search,Pageable pageable){

        return albumDAO.search(search,pageable);
    }
}
