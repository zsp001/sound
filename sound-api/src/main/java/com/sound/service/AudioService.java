package com.sound.service;



import com.sound.dao.AudioDAO;
import com.sound.model.AlbumModel;
import com.sound.model.AudioModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import com.sound.model.AudioModel;


@Service
public class AudioService extends BaseService<AudioModel>{

	@Autowired
	private AudioDAO audioDao;

	public List<AudioModel> findByAlbumId(@Param("albumId") Long albumId){

		return audioDao.findByAlbumId(albumId);
	}

	public Page<AudioModel> findByPage(Pageable pageable){
		return audioDao.findByPage(pageable);
	}

	public Page<AudioModel> search(String search,Pageable pageable){

		return audioDao.search(search,pageable);
	}
}
