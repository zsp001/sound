package com.sound.service;

import com.sound.controller.BaseController;
import com.sound.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2018/5/12.
 */
@Component
public class RedisService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedisTemplate<String, UserModel> redisTemplate;


    public final static String CODE = "_code";

    /**
     * redis中查询用户是否登陆
     * @param request
     * @return
     */
    public boolean isLogin(HttpServletRequest request){
        String key = BaseController.getCookieByName(request);
        System.out.println("key:"+key);
        UserModel userModel = redisTemplate.opsForValue().get(key);

        if(userModel == null){
            return false;
        }else{
            if(userModel.getId()<=0){
                return false;
            }else{
                return true;
            }
        }
    }

    /**
     * redis中获取用户
     * @param request
     * @return
     */
    public UserModel getUserInfo(HttpServletRequest request){
        String key = BaseController.getCookieByName(request);
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 存储用户信息
     * @param ssoToken
     * @param user
     */
    public void saveUser(String ssoToken,UserModel user){

        if(stringRedisTemplate.hasKey(user.getUserId())){
            String reSsoToken = stringRedisTemplate.opsForValue().get(user.getUserId());
            if(!ssoToken.equals(reSsoToken)){
//                redisTemplate.opsForValue().set(ssoToken,user,1, TimeUnit.DAYS);
//                stringRedisTemplate.opsForValue().set(user.getUserId(),ssoToken,1, TimeUnit.DAYS);
                redisTemplate.opsForValue().set(ssoToken,user);
                stringRedisTemplate.opsForValue().set(user.getUserId(),ssoToken);
            }

        }else{
//            redisTemplate.opsForValue().set(ssoToken,user,1, TimeUnit.DAYS);
//            stringRedisTemplate.opsForValue().set(user.getUserId(),ssoToken,1, TimeUnit.DAYS);
            redisTemplate.opsForValue().set(ssoToken,user);
            stringRedisTemplate.opsForValue().set(user.getUserId(),ssoToken);
        }

    }

    public void refresh(HttpServletRequest request){
        String ssoToken = BaseController.getCookieByName(request);
        UserModel user =redisTemplate.opsForValue().get(ssoToken);
        redisTemplate.opsForValue().set(ssoToken,user);
        stringRedisTemplate.opsForValue().set(user.getUserId(),ssoToken);
    }
    /**
     *移除用户信息
     */
    public void removeUser(String ssoToken){
        redisTemplate.delete(ssoToken);
    }

    public void saveCode(String code,String userId){
        //redisTemplate.opsForValue().set(code,null,10*60,TimeUnit.SECONDS);
        stringRedisTemplate.opsForValue().set(userId+CODE,code,10, TimeUnit.MINUTES);
    }

    public String getCode(String userId){
        //redisTemplate.opsForValue().set(code,null,10*60,TimeUnit.SECONDS);
        return stringRedisTemplate.opsForValue().get(userId+CODE);
    }

    public void loginout(String userId){
        stringRedisTemplate.delete(userId+CODE);
        String ssotoken = stringRedisTemplate.opsForValue().get(userId);
        stringRedisTemplate.delete(userId);
        redisTemplate.delete(ssotoken);
    }
}
