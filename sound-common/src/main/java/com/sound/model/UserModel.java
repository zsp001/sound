package com.sound.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Entity(name="userinfo") // 构建数据库表实体
@Table(name = "user",indexes = {@Index(name="k0", columnList="userId,password"),@Index(name="k1", columnList="ticket",unique = true)})
public class UserModel implements Serializable {

	private static final long serialVersionUID = -3946734305303957850L;

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;//主键

	@Column(columnDefinition="varchar(255) default ''")
    private String ticket;//事务ID


	@ApiModelProperty(hidden = true)
    @Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createTime; //插入时间

	@Column(columnDefinition="varchar(255) default ''")
	private String name;//用户昵称

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition="varchar(255) default ''")
	private String headerUrl;//人头像

	@Column(columnDefinition="varchar(255) default ''")
	private String headFileName;

    @NotBlank(message="userId不能为空")
    @Column(length = 32)
    private String userId;//账号

    @NotBlank(message="password不能为空")
    @Column(length = 32)
    private String password;//密码

	@Column(columnDefinition="varchar(64) default ''")
    private String baiduAccount;//百度帐号

	@Column(columnDefinition="varchar(32) default ''")
    private String phoneNum;//手机号

	@Column(columnDefinition="Int(11) default 0")
    private Integer role;//0为普通用户；1为管理员；

	@Column(columnDefinition="tinyint(1) default 0")
	private Boolean pushSysMsg;//是否打开推送系统消息
	
	@Column(columnDefinition="varchar(255) default 0")
	private String nowDeviceId;//是否打开推送系统消息
    
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBaiduAccount() {
		return baiduAccount;
	}

	public void setBaiduAccount(String baiduAccount) {
		this.baiduAccount = baiduAccount;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public Boolean isPushSysMsg() {
		return pushSysMsg;
	}

	public void setPushSysMsg(Boolean pushSysMsg) {
		this.pushSysMsg = pushSysMsg;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeaderUrl() {
		return headerUrl;
	}

	public void setHeaderUrl(String headerUrl) {
		this.headerUrl = headerUrl;
	}

	public String getHeadFileName() {
		return headFileName;
	}

	public void setHeadFileName(String headFileName) {
		this.headFileName = headFileName;
	}

	public Boolean getPushSysMsg() {
		return pushSysMsg;
	}

	public String getNowDeviceId() {
		return nowDeviceId;
	}

	public void setNowDeviceId(String nowDeviceId) {
		this.nowDeviceId = nowDeviceId;
	}
	
}

