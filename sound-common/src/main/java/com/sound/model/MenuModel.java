package com.sound.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by Administrator on 2018/5/6.
 */
@Entity(name = "menu")
// 构建数据库表实体
@Table(name = "menu")
public class MenuModel {

    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="varchar(255)  NOT null")
    private String menuName;

    @Column(columnDefinition="varchar(255)  NOT null")
    private String path;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
