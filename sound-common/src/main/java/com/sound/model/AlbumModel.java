package com.sound.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity(name = "album")
@Table(name = "album") // 音频专辑表
@ApiModel
public class AlbumModel {
	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 64)
	private String ticket;

	@Column(columnDefinition="Int(11) default 0")
	private Integer priority;

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createTime;//创建时间
	
	@Column(columnDefinition="varchar(255)  NOT null")
	@NotBlank(message="专辑名不能为空")
    private String name;//专辑名

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition="varchar(255)  NOT null")
    private String imgMinUrl;//栏目图片

	@ApiModelProperty(hidden = true)
    private String imgMaxUrl;//系统推荐图片

	@Column(columnDefinition="text NOT null")
	@NotBlank(message="摘要不能为空")
	private String summary;//摘要

	@Column(columnDefinition="varchar(255)  NOT null")
	@NotBlank(message="专辑标题不能为空")
    private String title;//专辑标题

	@Column(columnDefinition="varchar(255)  NOT null")
    private String type;//专辑类型

	@Column(columnDefinition="bigint(20) DEFAULT 0")
	private Long typeId;

	@Column(columnDefinition="bigint(20) DEFAULT 0")
	private Long authorId;//专辑作者

	@NotBlank(message="专辑作者不能为空")
	private String authorName;

	@Column(columnDefinition="varchar(255)  DEFAULT ''")
	private String tag;//节目标签

	@Column(columnDefinition="bigint(20) DEFAULT 0")
	private Long playCount;

	@Column(columnDefinition="bigint(20) DEFAULT 0")
	private Long audioCount;
	
	@Column
	private Boolean display; //展示状态
	
	
	public Long getAudioCount() {
		return audioCount;
	}

	public void setAudioCount(Long audioCount) {
		this.audioCount = audioCount;
	}

	public Long getPlayCount() {
		return playCount;
	}

	public void setPlayCount(Long playCount) {
		this.playCount = playCount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgMinUrl() {
		return imgMinUrl;
	}

	public void setImgMinUrl(String imgMinUrl) {
		this.imgMinUrl = imgMinUrl;
	}

	public String getImgMaxUrl() {
		return imgMaxUrl;
	}

	public void setImgMaxUrl(String imgMaxUrl) {
		this.imgMaxUrl = imgMaxUrl;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Boolean getDisplay() {
		return display;
	}

	public void setDisplay(Boolean display) {
		this.display = display;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
}
