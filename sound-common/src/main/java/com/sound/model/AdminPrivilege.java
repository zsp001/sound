package com.sound.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity // 管理员_权限关系表
@Table(name = "admin_privilege")
public class AdminPrivilege {

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Integer aId;  //管理员id

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getaId() {
		return aId;
	}

	public void setaId(Integer aId) {
		this.aId = aId;
	}

	public Integer getpId() {
		return pId;
	}

	public void setpId(Integer pId) {
		this.pId = pId;
	}

	private Integer pId;  //权限id


}
