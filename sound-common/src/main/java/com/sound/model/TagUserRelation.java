package com.sound.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

@Entity // 标签表
@Table(name = "tag_relation")
public class TagUserRelation {

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(columnDefinition="varchar(64) default ''")
	private String ticket;

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createTime;//插入时间
    
    private int tagName;//tag名字
    
    private String objType;//是文章还是音频
    
    private String objId;//被打tag对象的id

}
