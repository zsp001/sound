package com.sound.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * 栏目model
 * @author Administrator
 *
 */

@Entity // 栏目表
@Table(name = "soundColumn")
public class ColumnModel {

	@Column(columnDefinition="varchar(255)  NOT null")
	@NotBlank(message="栏目名不能为空")
	private String colName;//栏目名称

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
}
