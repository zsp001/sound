package com.sound.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

@Entity // 标签表
@Table(name = "type_user_relation")
public class TypeUserRelation {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(columnDefinition="varchar(64) default ''")
	private String ticket;

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createTime;//插入时间

	@Column(columnDefinition="int(11) default 0")
    private Integer typeName;//type名字

	@Column(columnDefinition="varchar(255) default ''")
    private String objType;//是文章还是音频

	@Column(columnDefinition="varchar(255) default ''")
    private String objId;//被打tag对象的id

}
