package com.sound.model;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import java.util.Date;

@Entity(name="collect") // 收藏信息表
@Table(name = "sound_collect" ,uniqueConstraints = { @UniqueConstraint(columnNames = { "userId","objId","isAlbum" }) } )
public class CollectModel {

	@ApiModelProperty(hidden = true)
	@Id
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@ApiModelProperty(hidden = true)
	@Column(length = 64)
	private String ticket;

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createTime;	//创建时间

	@NotBlank(message="userId不能为空")
    private String userId;	//用户ID
	
	@NotNull(message="bigTypeId不能为空")
	private Long bigTypeId;//0-1-2-3客户端自己定，留一个特殊号给专辑

	@Column(columnDefinition="bigint(20) DEFAULT 0")
	private Long objId;//
	
	@NotNull(message="isAlbum不能为空")
	private Boolean isAlbum ;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}




	public Long getBigTypeId() {
		return bigTypeId;
	}

	public void setBigTypeId(Long bigTypeId) {
		this.bigTypeId = bigTypeId;
	}

	public Boolean getIsAlbum() {
		return isAlbum;
	}

	public void setIsAlbum(Boolean isAlbum) {
		this.isAlbum = isAlbum;
	}

	public Long getObjId() {
		return objId;
	}

	public void setObjId(Long objId) {
		this.objId = objId;
	}
	
	


}
