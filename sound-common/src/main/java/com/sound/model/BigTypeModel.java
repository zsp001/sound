package com.sound.model;


import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Created by Administrator on 2018/4/27.
 */
@Entity(name="bigType")
@Table(name="sound_bigType")
public class BigTypeModel{

    @ApiModelProperty(hidden = true)
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message="大栏目名称")
    @Column(columnDefinition="varchar(100)  NOT null")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
