package com.vo;

import java.util.Date;
import java.util.List;

import com.sound.common.Base;
import com.sound.model.AudioModel;

public class AlbumVo extends Base{

	private Long id;
	
	private String ticket;

    private Date createTime;//创建时间

    private String name;//专辑名

    private String imgMinUrl;//栏目图片
    
    private String imgMaxUrl;//系统推荐图片
    
    private String title;//专辑标题
    
    private String type;//专辑类型
    
    private List<AudioModel> albumList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImgMinUrl() {
		return imgMinUrl;
	}

	public void setImgMinUrl(String imgMinUrl) {
		this.imgMinUrl = imgMinUrl;
	}

	public String getImgMaxUrl() {
		return imgMaxUrl;
	}

	public void setImgMaxUrl(String imgMaxUrl) {
		this.imgMaxUrl = imgMaxUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<AudioModel> getAlbumList() {
		return albumList;
	}

	public void setAlbumList(List<AudioModel> albumList) {
		this.albumList = albumList;
	}

    
    
}
