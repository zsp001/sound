package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.RoleDao;
import com.dao.UserShiroDao;
import com.sound.model.Permission;
import com.sound.model.Role;
import com.sound.model.UserShiro;

@Service
public class UserShiroService extends BaseService<UserShiro> {

	@Autowired
	private UserShiroDao userShiroDao;

	@Autowired
	private RoleDao roleDao;

	/**
	 * 根据用户名查询
	 */
	public UserShiro findByName(String name) {
		return userShiroDao.findByName(name);
	}

	/**
	 * 添加用户
	 */
	public UserShiro addUserShiro(Map<String, Object> map) {
		UserShiro userShiro = new UserShiro();
		userShiro.setName(map.get("name").toString());
		userShiro.setPassword(map.get("password").toString());
		userShiroDao.save(userShiro);
		return userShiro;
	}

	/**
	 * 添加角色
	 */
	public Role addRole(Map<String, Object> map) {
		UserShiro userShiro = userShiroDao.getOne(Long.valueOf(map.get("id").toString()));
		Role role = new Role();
		role.setRoleName(map.get("roleName").toString());
		role.setUserShiro(userShiro);
		Permission permission1 = new Permission();
		permission1.setPermission("create");
		permission1.setRole(role);
		Permission permission2 = new Permission();
		permission2.setPermission("update");
		permission2.setRole(role);
		List<Permission> permissions = new ArrayList<Permission>();
		permissions.add(permission1);
		permissions.add(permission2);
		role.setPermissions(permissions);
		roleDao.save(role);
		return role;
	}
}
