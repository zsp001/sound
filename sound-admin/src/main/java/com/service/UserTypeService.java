package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserTypeDAO;
import com.sound.model.UserTypeModel;

@Service
public class UserTypeService extends BaseService<UserTypeModel>{

	@Autowired
	private UserTypeDAO userTypeDao;

	/**
	 * 根据用户id查找自定义类型
	 * @param userId
	 * @return
	 */
	public List<UserTypeModel> findByUserId(String userId) {
		return userTypeDao.findByUserId(userId);
	}

	
}
