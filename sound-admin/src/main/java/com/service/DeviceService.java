package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.DeviceDAO;
import com.sound.model.Device;

/**
 * Created by Administrator on 2018/5/4.
 */
@Service
public class DeviceService extends BaseService<Device>{
	
	@Autowired
	private DeviceDAO deviceDao;
	
	/**
	 * 根据用户id查找设备
	 * @param userId
	 * @return
	 */
	public List<Device> findByUserId(String userId) {
		return deviceDao.findByUserId(userId);
	}
	
}
