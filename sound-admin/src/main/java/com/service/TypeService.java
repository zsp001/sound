package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.dao.TypeDAO;
import com.sound.model.AudioModel;
import com.sound.model.TypeModel;

@Service
public class TypeService extends BaseService<TypeModel> { 
	
	@Autowired
	private TypeDAO typeDao;
	
	public List<TypeModel> findByPriority(@Param("priority") Integer priority){
		return typeDao.findByPriority(priority);
	}
	
}