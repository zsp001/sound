package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.dao.AlbumDAO;
import com.sound.model.AlbumModel;

@Service
public class AlbumService extends BaseService<AlbumModel>{

	@Autowired
	private AlbumDAO<AlbumModel> repository;
	
	public List<AlbumModel> findByPriority(@Param("priority") Integer priority){
		return repository.findByPriority(priority);
	}
	
	public Integer updatePriority(@Param("priority") Integer priority,@Param("id") Long id){
		
		return repository.updatePriority(priority,id);
	}
//	
//	public AlbumModel findById(Long id) {
//		// TODO Auto-generated method stub
//		
//		return repository.findById(id);
//	}
//
//	
//	public Object save(AlbumModel t) {
//		// TODO Auto-generated method stub
//		return super.save(t);
//	}
//
//	
//	public Iterable<AlbumModel> findAll() {
//		// TODO Auto-generated method stub
//		return super.findAll();
//	}
//
//	
//	public void deleteById(Long id) {
//		// TODO Auto-generated method stub
//		super.deleteById(id);
//	}
//
//	
//	public Page<AlbumModel> findAll(Pageable pageable) {
//		// TODO Auto-generated method stub
//		Page<AlbumModel> page= (Page<AlbumModel>) repository.findAll(pageable);
//		return repository.findAll(pageable)
//	}

	 public Iterable<AlbumModel> findAlbumByDispay(){
	        return repository.findAlbumByDispay();
	    }
}
