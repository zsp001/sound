package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.dao.AudioDAO;
import com.sound.model.AlbumModel;
import com.sound.model.AudioModel;

@Service
public class AudioService extends BaseService<AudioModel>{

	@Autowired
	private AudioDAO audioDao;

	public List<AudioModel> findByAlbumId(@Param("albumId") Long albumId){

		return audioDao.findByAlbumId(albumId);
	}
	
	public List<AudioModel> findByPriority(@Param("priority") Integer priority){
		return audioDao.findByPriority(priority);
	}
}
