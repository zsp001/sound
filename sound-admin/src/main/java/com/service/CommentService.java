package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.dao.CommentDAO;
import com.sound.model.CommentModel;

@Service
public class CommentService extends BaseService<CommentModel>{

	@Autowired
	private CommentDAO commentDao;
	
	public List<CommentModel> findByNewsId(@Param("newsId") Long newsId){

		return commentDao.findByNewsId(newsId);
	}
}
