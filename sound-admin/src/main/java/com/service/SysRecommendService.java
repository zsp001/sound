package com.service;

import com.dao.SysRecommendDAO;
import com.sound.model.SystemRecommend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

/**
 * Created by Administrator on 2018/4/26.
 */
@Service
@Transactional
public class SysRecommendService extends BaseService<SystemRecommend>{

    @Autowired
    private SysRecommendDAO sysRecommendDAO;
    //根据优先级排序
    public List<SystemRecommend> findByPrio(){
        return sysRecommendDAO.findByPrio();
    }
    
    public Integer deletebyAlbumId(Long albumId){
        return sysRecommendDAO.deleteByAlbumId(albumId);
    }
}
