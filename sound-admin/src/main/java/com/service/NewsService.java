package com.service;

import com.dao.NewsDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.sound.model.NewsModel;

@Service
public class NewsService extends BaseService<NewsModel>{

    @Autowired
    private NewsDAO newsDAO;

    public Page<NewsModel> findByPage(Pageable pageable){
        return newsDAO.findByPage(pageable);

    }
    
	public Object [] findByPageAndNewsId(@Param("page") Integer p,@Param("size") Integer size,@Param("newsId") Integer newsId){
		p = p*size;
		System.out.println(p);
		return newsDAO.findByPageAndNewsId(p, size ,newsId);
	}

}
