package com.controller;


import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.service.ColumnService; 
import com.service.TypeService;
import com.sound.common.ParamCode;
import com.sound.common.QiNiuConfig;
import com.sound.common.Response;
import com.sound.model.AlbumModel;
import com.sound.model.AudioModel;
import com.sound.model.TypeModel;

import io.swagger.annotations.Api;

/**
 * 类型操作类
 * @author Administrator
 *
 */
@Controller
@Api(tags="类型")
@RequestMapping(path="/type")
public class TypeController extends BaseController<TypeModel>{

	@Autowired
	private ColumnService columnService;
 
	@Autowired
	public QiNiuConfig qiNiuConfig;
	
	@Autowired
	private TypeService typeService;
	
	@GetMapping(path="/findById")
	@ResponseBody
	@Override
	public Response<TypeModel> findById(Long id) {

		return super.findById(id);
	}
	
	@PostMapping("/save")
	@ResponseBody
	public Response<Void> save(@ModelAttribute @Valid TypeModel t, BindingResult result) {
		System.out.println(t.getBigTypeName());
		System.out.println(t.getLogoUrl());
		System.out.println(t.getName());
		System.out.println(t.getBigTypeId());
		return super.save(t, result);
	}
	
	@PostMapping(path="/saveImg")
	@ResponseBody
	public Response saveImg(@RequestParam("file") MultipartFile file) {
		Response resp = new Response();
		if(file.isEmpty()){
			resp.setRespone(ParamCode.FAIL);
			resp.setMsg("文件为空");
			return resp;
		}
		//上传文件到七牛云
		File file1 = null;
		String key = null;
		try {
			file1 = File.createTempFile(file.getName(),".tmp");
			file.transferTo(file1);
			key= qiNiuConfig.upLoadImg(file1);
		} catch (IOException e) {
			e.printStackTrace();
		}
//		if(columnService.findById(t.getCloId())==null) {
//			return new Response<Void>(false);
//		}
//		t.setLogoUrl(key);
//		t.setLogoName(file.getOriginalFilename());
	//	return super.save(t, result);
		resp.setData(key);
		System.out.println(key+"asdasd");
		return resp;
	}
	
	@GetMapping(path="/findAll")
	@ResponseBody
	@Override
	public Response<Iterable<TypeModel>> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}
	
	@GetMapping(path="/deleteById")
	@ResponseBody
	@Override
	public Response<Void> deleteById(Long id) {
		// TODO Auto-generated method stub
		return super.deleteById(id);
	}

	@GetMapping(path="/findByPage")
	@ResponseBody
	@Override
	public Response<Page<TypeModel>> findByPage(int page, int size) {
		return super.findByPage(page, size);
	}
	
	
	/**
	 * 更改推荐级别
	 */
	@PostMapping(path="/updatePriority")
	@ResponseBody
	public Response<Void> updatePriority(@ModelAttribute @Valid TypeModel t,BindingResult result){
		System.out.println(t.getPriority());
		return super.save(t,result);
	}
	
	/**
	 * 根据推荐等级查询
	 * @param priority
	 * @return
	 */
	@GetMapping("/findByPriority")
	@ResponseBody
	public Response<List<TypeModel>> findByPriority(@Param("priority") Integer priority) {
		Response<List<TypeModel>> resp;
		if (priority.toString()==null) {
			 resp=new Response<List<TypeModel>>(false);
			resp.setMsg("参数未传递");
			return resp;
		}else if (priority<0||priority>2) {
			 resp=new Response<List<TypeModel>>(false);
			resp.setMsg("参数不合法");
		}
		resp=new Response<List<TypeModel>>(true);
		resp.setData(typeService.findByPriority(priority));
		return resp;
	}
}

 
