package com.controller;

import javax.validation.Valid;


import com.sound.common.Response;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sound.model.TagModel;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Api(tags="标签")
@RequestMapping(path="/tag")
public class TagController extends BaseController<TagModel>{


	@GetMapping(path="/findById")
	@ResponseBody
	@Override
	public Response<TagModel> findById(Long id) {
		return super.findById(id);
	}
	
	@PostMapping(path="/save")
	@ResponseBody
	@Override
	public Response<Void> save(@ModelAttribute @Valid TagModel t, BindingResult result) {

		return super.save(t, result);
	}
	
	@GetMapping(path="/findAll")
	@ResponseBody
	@Override
	public Response<Iterable<TagModel>> findAll() {
		// TODO Auto-generated method stub
		return super.findAll();
	}
	
	@GetMapping(path="/deleteById")
	@ResponseBody
	@Override
	public Response<Void> deleteById(Long id) {
		// TODO Auto-generated method stub
		return super.deleteById(id);
	}

	@GetMapping(path="/findByPage")
	@ResponseBody
	@Override
	public Response<Page<TagModel>> findByPage(int page, int size) {
		return super.findByPage(page, size);
	}


}
