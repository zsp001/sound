package com.controller;

import com.sound.common.ParamCode;
import com.sound.common.QiNiuConfig;
import com.sound.common.Response;
import com.sound.model.NewsModel;
import com.sound.model.SystemMsgModel;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/5/2.
 */

/**
 * 系统消息
 */
@Controller
@Api(tags="系统消息")
@RequestMapping(path="/sysmsg")
public class SystemMsgController extends BaseController<SystemMsgModel>{


    @Autowired
    public QiNiuConfig qiNiuConfig;

    @GetMapping(path = "/findById")
    @ResponseBody
    @Override
    public Response<SystemMsgModel> findById(Long id) {
        return super.findById(id);
    }
    @PostMapping(path ="/saveImage")
    @ResponseBody
    public  Response<List> save(@RequestParam("file") MultipartFile file) {
    	List<String> list  =null;
        Response<List> resp = new Response<>();
        if(!file.isEmpty()){
            File file1 = null;
            String key = null;
            try {
                file1 = File.createTempFile(file.getName(),".tmp");
                file.transferTo(file1);
                key= qiNiuConfig.upLoadImg(file1);
                list = new ArrayList<String>();
                list.add(key);
                list.add(file.getOriginalFilename());
                System.out.println(file.getName());
                resp.setCode("000000");
                resp.setMsg("图片上传成功！");
                resp.setData(list);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resp;
    }
    @PostMapping(path = "/save")
    @ResponseBody
    public Response<Void> save(@ModelAttribute SystemMsgModel systemMsgModel,BindingResult result) {
        return super.save(systemMsgModel, result);
    }

    @GetMapping(path = "/findAll")
    @ResponseBody
    @Override
    public Response<Iterable<SystemMsgModel>> findAll() {
        return super.findAll();
    }
    @GetMapping(path = "/deleteById")
    @ResponseBody
    @Override
    public Response<Void> deleteById(Long id) {
        return super.deleteById(id);
    }
    @GetMapping(path="/findByPage")
    @ResponseBody
    @Override
	public Response<Page<SystemMsgModel>> findByPage(@RequestParam int page,@RequestParam int size) {
		return super.findByPage(page, size);
	}

}
