package com.controller;

import com.sound.common.ParamCode;
import com.sound.common.Response;
import com.sound.model.Admin;
import com.sound.model.NewsModel;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.util.DigestUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * Created by Administrator on 2018/5/5.
 */
@RestController
@RequestMapping(path="/admin")
public class AdminController extends BaseController<Admin>{

    @GetMapping(path="/findById")
    @Override
    public Response<Admin> findById(Long id) {
        return super.findById(id);
    }

    @GetMapping(path="/save")
    @Override
    public Response<Void> save(Admin admin, BindingResult result) {
        return super.save(admin, result);
    }

    @GetMapping(path="/findAll")
    @Override
    public Response<Iterable<Admin>> findAll() {
        return super.findAll();
    }

    @GetMapping(path="/deleteById")
    @Override
    public Response<Void> deleteById(Long id) {
        return super.deleteById(id);
    }

    @GetMapping(path="/login")
    public Response<Boolean> login(HttpServletRequest request, HttpServletResponse req,
                                   String username , String password){
        Response<Boolean> resp = new Response<Boolean>();
        Admin admin = new Admin();
        admin.setUserName(username);
        admin.setPasswd(password);
        Example<Admin> t=  Example.of(admin);
        Iterable<Admin> result = super.service.findAll(t);
        Admin admin1= result.iterator().next();
        if(admin1!=null && admin1.getId()<=0){
            resp.setRespone(ParamCode.FAIL);
            resp.setMsg("用户密码或者账号不正确");
            return resp;
        }else{
            HttpSession session = request.getSession();
            String token = "";
            try {
                token = DigestUtils.md5DigestAsHex((""+admin1.getId()+new Date().getTime()).getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            session.setAttribute(token,admin1);
            Cookie cookie = new Cookie("ssoToken",token);
            cookie.setPath("/");
            req.addCookie(cookie);
            admin1.setPasswd("");
            resp.setRespone(ParamCode.SUCSESS);
        }
        return resp;
    }
    @GetMapping(path="/findByPage")
	public Response<Page<Admin>> findByPage(@RequestParam int page,@RequestParam int size) {
		return super.findByPage(page, size);
	}

}
