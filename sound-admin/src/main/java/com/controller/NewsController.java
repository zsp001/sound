package com.controller;

import javax.validation.Valid;

import com.sound.common.QiNiuConfig;
import com.sound.common.Response;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.sound.common.ParamCode;
import com.sound.model.NewsModel;
import com.sound.model.SystemMsgModel;
import com.sound.model.UserModel;
import com.sound.model.Admin;
import com.sound.model.CommentModel;
import com.sound.model.NewsContentModel;
import com.service.NewsContentService;
import com.service.NewsService;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 资讯
 * @author Administrator
 *
 */

@RestController
@Api(tags="资讯")
@RequestMapping(path="/news")
public class NewsController extends BaseController<NewsModel>{

	private static final Logger logger = LoggerFactory.getLogger(NewsController.class);

	@Autowired
	private NewsContentService newsContentService;
	@Autowired
	private NewsService newsService;

	@Autowired
	public QiNiuConfig qiNiuConfig;

	/**
	 * 查询单条咨询记录
	 */
	@GetMapping("/findById")
	public Response<NewsModel>  findById(@RequestParam(required=true) Long id) {
		Response<NewsModel> resp = new Response<NewsModel>();
		try {
			NewsModel news = newsService.findById(id);
			if(news != null) {//获取咨询内容
				NewsContentModel newc = newsContentService.findById(news.getContentId());
				news.setContent(newc.getContent());
			}else {
				resp.setRespone(ParamCode.FAIL);
				resp.setMsg("咨询不存在！");	
			}
			resp.setData(news);
		} catch (Exception e) {
			logger.error("news findById fail!", e);
		}
		 return resp;
	}
	
	/**
	 * 保存单条咨询记录
	 */
	 	@PostMapping(path = "/save")
	    public Response<Void> save(@ModelAttribute NewsModel newsModel,BindingResult result) {
		 	NewsContentModel contentModel = new NewsContentModel();
		 	contentModel.setContent(newsModel.getContent());
		 	contentModel = newsContentService.save(contentModel);
		 	newsModel.setContentId(contentModel.getId());
	        return super.save(newsModel, result);
	    }
	 
	 /**
	  * 保存图片
	  * @param file
	  * @return
	  */
	 	@PostMapping(path ="/saveImage")
	    public  Response<List> save(@RequestParam("file") MultipartFile file) {
	    	List<String> list  =null;
	        Response<List> resp = new Response<>();
	        if(!file.isEmpty()){
	            File file1 = null;
	            String key = null;
	            try {
	                file1 = File.createTempFile(file.getName(),".tmp");
	                file.transferTo(file1);
	                key= qiNiuConfig.upLoadImg(file1);
	                list = new ArrayList<String>();
	                list.add(key);
	                list.add(file.getOriginalFilename());
	                resp.setCode("000000");
	                resp.setMsg("图片上传成功！");
	                resp.setData(list);
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	        return resp;
	    }
	 
	/**
	 * 查询所有
	 */
	@GetMapping(path="/findAll")
    public Response<Iterable<NewsModel>> findAll() {
        return super.findAll();
    }

	/**
	 * 删除咨询记录
	 */
	@GetMapping("/deleteById")
	@Override
	public Response<Void> deleteById(Long id) {
		try {
			NewsModel nes = newsService.findById(id);
			if(nes==null) {
				return new Response<Void>(true);
			}
			newsContentService.deleteById(nes.getContentId());
			newsService.deleteById(id); //删除咨询内容
			return new Response<Void>(true);
		} catch (Exception e) {
			logger.error("new deleteById fail", e);
			
		}
		return new Response<Void>(false);
		
	}

	@GetMapping(path="/findByPage")
	public Response<Page<NewsModel>> findByPage(@RequestParam int page,@RequestParam int size) {
		return super.findByPage(page, size);
	}

	@GetMapping(path="/findByPageAndNewsId")
	public Object[] findByPageAndNewsId(@RequestParam Integer page,@RequestParam Integer size,@RequestParam Integer newsId) {
		System.out.println(page);
		return newsService.findByPageAndNewsId(page, size ,newsId);
	}

}
