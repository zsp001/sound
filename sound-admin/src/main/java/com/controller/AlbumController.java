package com.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import com.sound.common.QiNiuConfig;
import com.sound.common.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.sound.common.ParamCode;
import com.sound.model.AlbumModel;
import com.service.AlbumService;
import com.service.AudioService;
import com.vo.AlbumVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 专辑控制类
 * @author Administrator
 *
 */
@Controller
@Api(tags="专辑")
@RequestMapping(path="/album")
public class AlbumController extends BaseController<AlbumModel>{

	@Autowired
	private AudioService audioService;
	
	@Autowired
	private AlbumService albumService;

	@Autowired
	public QiNiuConfig qiNiuConfig;

	/**
	 * 查看专辑详情
	 */
	@GetMapping(path="/findById")
	@ResponseBody
	public Response findById(Long id) {
		Response<AlbumVo> resp = new Response<>(true);
		
		AlbumModel model = albumService.findById(id);
		if(model==null) {
			 resp.setRespone(ParamCode.FAIL);
			 return resp;
		}
		AlbumVo vo = new AlbumVo();
		vo.setCreateTime(model.getCreateTime());
		vo.setId(model.getId());
		vo.setImgMaxUrl(model.getImgMaxUrl());
		vo.setImgMinUrl(model.getName());
		vo.setTicket(model.getTicket());
		vo.setTitle(model.getTitle());

		vo.setType(model.getType());
		vo.setAlbumList(audioService.findByAlbumId(id));
		resp.setData(vo);
		return resp;
	}

	/**
	 * 新增专辑或者更新专辑
	 */
	@PostMapping("/save")
	@ResponseBody
	public Response<Void> save(@ModelAttribute @Valid AlbumModel t, BindingResult result) {
		System.out.println(222222);
		return super.save(t, result);
	}
	
	/**
	 * 上传图片
	 */
	@PostMapping("/saveOneImg")
	@ResponseBody
	public Response<String> saveOneImg(@RequestParam("file") MultipartFile file) {
		Response<String> respone = new Response<String>();
		if(file.isEmpty()){
			respone.setRespone(ParamCode.FAIL);
			respone.setMsg("图片文件为空");
			return respone;
		}

		//上传文件到七牛云
		File file1 = null;
		String key = null;
		try {
			file1 = File.createTempFile(file.getName(),".tmp");
			file.transferTo(file1);
			key= qiNiuConfig.upLoadImg(file1);
		} catch (IOException e) {
			e.printStackTrace();
		}

		respone.setData(key);
		return respone;
	}
	

	@GetMapping(path="/findAll")
	@Override
	@ResponseBody
	public Response<Iterable<AlbumModel>> findAll() {
		
		return super.findAll();
	}

	/**
	 * 删除专辑
	 */
	@GetMapping(path="/deleteById")
	@ResponseBody
	@Override
	public Response<Void> deleteById(Long id) {
		
		return super.deleteById(id);
	}

	/**
	 * 分页查找专辑
	 * @param page
	 * @return
	 */
	@GetMapping(path="/findByPage")
	@ResponseBody
	public Response<Page<AlbumModel>> findByPage(@Param("page")int page,@Param("size")int size) {
		return super.findByPage(page,size);
	}
	
	/**
	 * 更新专辑推荐等级
	 */
	@GetMapping("/updatePriorityById")
	@ResponseBody
	public Response<Void> updatePriorityById(@Param("priority") Integer priority,@Param("id") Long id) {
		System.out.println(priority);
		Response<Void> resp;
		if (priority.toString()==null||id.toString()==null) {
			 resp=new Response<Void>(false);
			resp.setMsg("参数未传递");
			return resp;
		}else if (priority<0||priority>2) {
			 resp=new Response<Void>(false);
			 resp.setMsg("参数不合法");
		}
		resp=new Response<Void>(true);
		albumService.updatePriority(priority,id);
		return resp;
	}
	
	/**
	 * 根据推荐等级查询
	 * @param priority
	 * @return
	 */
	@GetMapping("/findByPriority")
	@ResponseBody
	public Response<List<AlbumModel>> findByPriority(@Param("priority") Integer priority) {
		System.out.println(priority);
		Response<List<AlbumModel>> resp;
		if (priority.toString()==null) {
			 resp=new Response<List<AlbumModel>>(false);
			resp.setMsg("参数未传递");
			return resp;
		}else if (priority<0||priority>2) {
			 resp=new Response<List<AlbumModel>>(false);
			resp.setMsg("参数不合法");
		}
		resp=new Response<List<AlbumModel>>(true);
		resp.setData(albumService.findByPriority(priority));
		return resp;
	}
	
	@GetMapping(path = "/findAlbumByDisplay")
	@ResponseBody
	@ApiOperation(value = "获取单个type下所有专辑",tags = "",response = AlbumModel.class)
	public Response<Iterable<AlbumModel>> findAlbumByDispay(){
		Response<Iterable<AlbumModel>> resp=new Response<Iterable<AlbumModel>>();
		Iterable<AlbumModel> iterable =albumService.findAlbumByDispay();
		resp.setData(iterable);
		return resp;
	}
	
}
