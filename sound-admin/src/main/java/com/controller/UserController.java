package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.sound.model.SystemMsgModel;
import com.sound.model.UserModel;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.sound.common.Response;


@RestController
@Api(tags="用户")
@RequestMapping(path="/user")
public class UserController extends BaseController<UserModel>{

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	/**
	 * 分页查询用户
	 * @param page
	 * @return
	 */
	@GetMapping(path="/findByPage")
	public Response<Page<UserModel>> findByPage(@RequestParam int page,@RequestParam int size) {
		return super.findByPage(page, size);
	}

	@GetMapping(path="/findByExamplePage/{page}/{size}")
	public Response<Page<UserModel>> findByExamplePage(@ModelAttribute @Valid UserModel userModelDTO, BindingResult result,@PathVariable int page, @PathVariable int size){
		return super.findByExamplePage(userModelDTO, result, page, size);
	}
	/**
	 * 查询所有用户
	 */
	 @GetMapping(path = "findAll")
	 public Response<Iterable<UserModel>> findAll() {
	      return super.findAll();
	 }
	/**
	 * 添加用户
	 * @param userModelDTO
	 * @param result
	 * @return
	 */
	@GetMapping(path="/save")
	@ResponseBody
	public Response<Void> save (@ModelAttribute @Valid UserModel userModelDTO, BindingResult result) {
		return super.save(userModelDTO, result);
	}

	@GetMapping(path="/ticket")
	public @ResponseBody
    Response<String> getTikect(){
		String now  = String.valueOf(System.currentTimeMillis());
		String rand = String.valueOf( Math.random()*10000000);
		Response<String> respDTO = new Response<String>(true);
		respDTO.setData(now+rand);
		return respDTO;
	}

	/**
	 * 根据id查询单个用户
	 * @param id
	 * @return
	 */
	@GetMapping(path="/findById")
	@Override
	public Response<UserModel> findById(@RequestParam Long id) {
		return super.findById(id);
	}

	@GetMapping(path="/deleteById")
	@Override
	public Response<Void> deleteById(@RequestParam Long id) {
		return super.deleteById(id);
	}

	/**
	 * 检查数据是否可用
	 */
//	@GetMapping(path="/check/{param}/{type}")
//	@ResponseBody
//	public Response<UserModel> checkUserData(String data,Integer type){
//		Response<UserModel> resp = userService.checkUserData(data, type);
//		return resp	;
//	}

	/**
	 * 用户登录
	 */
//	@GetMapping(path="/login")
//	public Response<UserModel> login(String username, String password, HttpServletRequest request, HttpServletResponse response){
//		//接受两个参数,调用service进行登录
//		Response<UserModel> resp = userService.login(username,password);
////		从返回结果中取回token,保存至cookie里
//		String token = resp.getData().toString();
//		return resp;
//	}
}
