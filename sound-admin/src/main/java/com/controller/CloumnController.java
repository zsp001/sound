package com.controller;

import javax.validation.Valid;

import com.sound.common.Response;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sound.model.ColumnModel;

/**
 * 栏目操作
 * @author Administrator
 *
 */
@Controller
@Api(tags="栏目")
@RequestMapping(path="/column")
public class CloumnController extends BaseController<ColumnModel>{

	
	@GetMapping("/findById")
	@ResponseBody
	public Response<ColumnModel> findById(Long id) {

		return super.findById(id);
	}

	@GetMapping("/save")
	@ResponseBody
	public Response<Void> save(@Valid ColumnModel t, BindingResult result) {

		return super.save(t, result);
	}

	@GetMapping("/findAll")
	@ResponseBody
	public Response<Iterable<ColumnModel>> findAll() {
	 
		return super.findAll();
	}

	@GetMapping("/deleteById")
	@ResponseBody
	@Override
	public Response<Void> deleteById(Long id) {
		return super.deleteById(id);
	}


	@GetMapping("/findByPage")
	@ResponseBody
	@Override
	public Response<Page<ColumnModel>> findByPage(int page, int size) {
		return super.findByPage(page, size);
	}


}
