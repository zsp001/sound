package com.controller;

import com.alibaba.fastjson.JSON;
import com.sound.common.ParamCode;
import com.sound.common.QiNiuConfig;
import com.sound.common.Response;
import com.sound.model.AlbumModel;
import com.sound.model.SystemRecommend;
import com.service.SysRecommendService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2018/4/26.
 */

@Controller
@Api(tags="系统推荐")
@RequestMapping(path="/recommend")
public class SysRecommdController extends BaseController<SystemRecommend>{

    @Autowired
    private SysRecommendService sysRecommendService;

    @Autowired
    public QiNiuConfig qiNiuConfig;

    @GetMapping(path="/findById")
    @ResponseBody
    @Override
    public Response<SystemRecommend> findById(Long id) {
        return super.findById(id);
    }

//    @PostMapping(path="/save")
//    @ResponseBody
//    public Response<Void> save(@ModelAttribute @Valid SystemRecommend systemRecommend,
//                               @RequestParam("file") MultipartFile file, BindingResult result) {
//        Response resp =new Response();
//        if(file.isEmpty()){
//            resp.setRespone(ParamCode.FAIL);
//            resp.setMsg("推片文件为空");
//            return resp;
//        }
//        //上传文件到七牛云
//        File file1 = null;
//        String key = null;
//        try {
//            file1 = File.createTempFile(file.getName(),".tmp");
//            file.transferTo(file1);
//            key= qiNiuConfig.upLoadImg(file1);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        systemRecommend.setBigimgUrl(key);
//        systemRecommend.setBigImgName(file.getOriginalFilename());
//        return super.save(systemRecommend, result);
//    }

    @PostMapping("/save")
	@ResponseBody
	public Response<Void> save(@ModelAttribute @Valid SystemRecommend t, BindingResult result) {
		System.out.println(222222);
		return super.save(t, result);
	}
    
    @GetMapping(path="/findAll")
    @ResponseBody
    @Override
    public Response<Iterable<SystemRecommend>> findAll() {
        return super.findAll();
    }

    @GetMapping(path="/deleteById")
    @ResponseBody
    @Override
    public Response<Void> deleteById(Long id) {
        return super.deleteById(id);
    }
    

    //推荐列表
    @GetMapping(path="/getRecommendList")
    @ResponseBody
    public Response findByPage() {
        Response respone = new Response(true);

        List<SystemRecommend> recommendList= sysRecommendService.findByPrio();
        respone.setData(recommendList);
        return respone;
    }
    
    @GetMapping(path="deleteByAlbumId")
    @ResponseBody
    public Response<Void> deleteByAlbumId(Long albumId){
    	Response<Void> resp;
    	if (albumId==-1) {
			resp=new Response<Void>(false);
			resp.setMsg("参数不合法");
			return resp;
		}
    	resp=new Response<Void>(true);
    	sysRecommendService.deletebyAlbumId(albumId);
    	return resp;
    }
}
