package com.controller;

import com.service.DeviceService;
import com.sound.common.Response;
import com.sound.model.Device;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/4.
 */
@RestController
@Api(tags="系统设备")
@RequestMapping(path="/device")
public class DeviceController extends BaseController<Device>{

	 @Autowired
	private DeviceService deviceService;


    @GetMapping(path="findById.do")
    @Override
    public Response<Device> findById(Long id) {
        return super.findById(id);
    }

    @GetMapping(path = "/save")
    @Override
    public Response<Void> save(Device device, BindingResult result) {
        return super.save(device, result);
    }
    @GetMapping(path = "/findAll")
    @Override
    public Response<Iterable<Device>> findAll() {
        return super.findAll();
    }
    @GetMapping(path = "/deleteById")
    @Override
    public Response<Void> deleteById(Long id) {
        return super.deleteById(id);
    }
    @GetMapping(path = "/findByPage")
    @Override
    public Response<Page<Device>> findByPage(int page, int size) {
        return super.findByPage(page, size);
    }
    
   
	
    /**
     * 根据用户id查找设备
     * @param userId
     * @return
     */
	@GetMapping(path="/findByUserId")
	public Response<List<Device>> findByUserId(String userId){
		Response<List<Device>> resp = new Response<>(true);
		List<Device> deviceList = deviceService.findByUserId(userId);
		System.out.println(deviceList);
		resp.setData(deviceList);
		return resp;
	}

}
