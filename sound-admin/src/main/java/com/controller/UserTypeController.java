package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.UserTypeService;
import com.sound.common.Response;
import com.sound.model.UserTypeModel;

@RestController
@RequestMapping("/userType")
public class UserTypeController extends BaseController<UserTypeModel>{

	@Autowired
	private UserTypeService userTypeService;

	/**
	 * 根据用户id查询类型
	 */	
	@GetMapping(path="/findByUserId")
	public Response<List<UserTypeModel>> findByUserId(String userId){
		Response<List<UserTypeModel>> resp = new Response<>(true);
		List<UserTypeModel> userTypeList = userTypeService.findByUserId(userId);
		resp.setData(userTypeList);
		return resp;
	}
}
