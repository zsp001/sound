package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.service.CommentService;
import com.service.NewsService;
import com.sound.common.ParamCode;
import com.sound.common.Response;
import com.sound.model.Admin;
import com.sound.model.CommentModel;

import io.swagger.annotations.Api;


@RestController
@Api(tags="评论")
@RequestMapping(path="/comment")
public class CommentController extends BaseController<CommentModel>{
	
	@Autowired
	private NewsService newsService;
	
	@Autowired
	private CommentService commentService;

	@GetMapping(path="/findById")

	public Response<CommentModel> findById(Long id) {
		return super.findById(id);
	}

	@GetMapping(path = "/findAll")

	public Response<Iterable<CommentModel>> findAll(@ModelAttribute CommentModel commentModel) {
		return super.findAll(commentModel);
	}
	
	@GetMapping(path="/deleteById")
	@Override
	public Response<Void> deleteById(Long id) {
		// TODO Auto-generated method stub
		return super.deleteById(id);
	}
	
	/**
	 * 根据资讯id获取所有评论
	 */
	@GetMapping(path="/findByNewsId")
	public Response<List<CommentModel>> findCommentByNewsId(Long newsId){
		Response<List<CommentModel>> resp = new Response<List<CommentModel>>(true);
		List<CommentModel> commentList = commentService.findByNewsId(newsId);
		resp.setData(commentList);
		return resp;
	}
	 @GetMapping(path="/findByPage")
		public Response<Page<CommentModel>> findByPage(@RequestParam int page,@RequestParam int size) {
			return super.findByPage(page, size);
		}
	
}
