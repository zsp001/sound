package com.controller;

import com.sound.model.UserModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2018/4/15.
 */

@Api(tags = "实例", description ="样本类",produces="服务方",consumes="调用方",
        authorizations =@Authorization( value="zhoushuiping"))
@RestController
@RequestMapping(path="/simple")
public class SimpleController {
	private static final Logger logger = LoggerFactory.getLogger(SimpleController.class);

    @ApiOperation(value = "获取", notes = "获取hello word",response= UserModel.class)
    @RequestMapping(path="/ticket",method = RequestMethod.GET)
    @ResponseBody
    public String getHello(@RequestParam(required = false) String username){
    	try {
			
    		System.out.println("此处为业务逻辑代码！");
		} catch (Exception e) {
			logger.warn("simple/ticket exception", e);
		}

        return "hello word";
    }
}
