package com.controller;

import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sound.common.Response;
import com.sound.model.BigTypeModel;

import io.swagger.annotations.Api;

/**
 * Created by Administrator on 2018/4/27.
 */
@RestController
@Api(tags="大类型")
@RequestMapping(path="/bigType")
public class BigTypeController extends BaseController<BigTypeModel>{

    @GetMapping(path="/findById")
    @Override
    public Response<BigTypeModel> findById(Long id) {
        return super.findById(id);
    }

    @PostMapping(path="/save")
    @Override
    public Response<Void> save(@ModelAttribute BigTypeModel bigTypeModel, BindingResult result) {
    	System.out.println(bigTypeModel.getName());
    	return super.save(bigTypeModel, result);
    }

    @GetMapping(path="/findAll")
    @Override
    public Response<Iterable<BigTypeModel>> findAll() {
        return super.findAll();
    }

    @GetMapping(path="/deleteById")
    @Override
    public Response<Void> deleteById(Long id) {
        return super.deleteById(id);
    }

    @GetMapping(path="/findByPage")
    @Override
    public Response<Page<BigTypeModel>> findByPage( int page, int size) {
        return super.findByPage(page,size);
    }
}
