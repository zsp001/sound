package com.controller;

import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.service.UserShiroService;
import com.sound.model.Role;
import com.sound.model.UserShiro;

@Controller
@RequestMapping("/userShiro")
public class UserShiroController {

	@Autowired
	private UserShiroService userShiroService;

	// @RequestMapping(value = "/userlogin")
	// public String userLogin(Model model, UserShiro userShiro,
	// HttpServletResponse response) {
	// if (userShiro == null) {
	// return "login";
	// }
	// String account = userShiro.getName();
	// String password = userShiro.getPassword();
	// UsernamePasswordToken token = new UsernamePasswordToken(account,
	// password, false);
	// Subject currentUser = SecurityUtils.getSubject();
	// try {
	// currentUser.login(token);
	// // 此步将 调用realm的认证方法
	// } catch (IncorrectCredentialsException e) {
	// // 这最好把 所有的 异常类型都背会
	// model.addAttribute("message", "密码错误");
	// return "login";
	// } catch (AuthenticationException e) {
	// model.addAttribute("message", "登录失败");
	// return "login";
	// }
	// return "index";
	// }
	//
	// /**
	// * 登录
	// */
	// //配合shiro配置中的默认访问url
	// @RequestMapping(value="/login")
	// public String getLogin(HttpServletRequest request,Model model,HttpSession
	// session,HttpServletResponse response){
	// return "login";
	// }
	
	@GetMapping(value = "/login")
	public String login() {
 		return "login";
	}

	@PostMapping(value = "/login")
	public String login(String username, String password) {
		System.out.println(username);
		System.out.println(password);
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);
		SecurityUtils.getSubject().login(token);
 		return "index";
	}
	
	

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String home() {
		Subject subject = SecurityUtils.getSubject();
//		UserShiro principal = (UserShiro) subject.getPrincipal();
		System.out.println(subject);
		return "index";
	}


	// 退出
	@RequestMapping(value = "/logout")
	public String logout() {
		return "login";
	}

	// 错误页面展示
	@RequestMapping(value = "/error", method = RequestMethod.POST)
	public String error() {
		return "error ok!";
	}

	// 数据初始化
	@RequestMapping(value = "/addUser")
	public String addUser(@RequestBody Map<String, Object> map) {
		UserShiro userShiro = userShiroService.addUserShiro(map);
		return "添加用户成功! \n" + userShiro;
	}

	// 角色初始化
	@RequestMapping(value = "/addRole")
	public String addRole(@RequestBody Map<String, Object> map) {
		Role role = userShiroService.addRole(map);
		return "添加角色成功! \n" + role;
	}

	@RequiresRoles("admin")
	@RequiresPermissions("create")
	@RequestMapping(value = "/create")
	public String create() {
		return "Create success!";
	}

	@RequestMapping(value = "/403")
	public String unAuth() {
		return "权限不足";
	}
}
