package com.controller;

import com.sound.common.Response;
import com.sound.model.MenuModel;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2018/5/6.
 */
@RestController
@Api(tags = "菜单")
@RequestMapping(path = "/menu")
public class MenuController extends BaseController<MenuModel>{

    @GetMapping(path = "/findById")
    @Override
    public Response<MenuModel> findById(Long id) {
        return super.findById(id);
    }

    @GetMapping(path = "/findAll")
    @Override
    public Response<Iterable<MenuModel>> findAll() {
        return super.findAll();
    }

    @GetMapping(path = "/deleteById")
    @Override
    public Response<Void> deleteById(Long id) {
        return super.deleteById(id);
    }

}
