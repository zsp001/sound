package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sound.model.AudioModel;

@Repository
public interface AudioDAO extends JpaRepository<AudioModel,Long>{
	
	@Query("select u from audio u where u.albumId=:albumId")
	@Modifying
	public List<AudioModel> findByAlbumId(@Param("albumId") Long albumId);
	
	@Query("select u from audio u where u.priority=:priority")
	@Modifying
	public List<AudioModel> findByPriority(@Param("priority") Integer priority);
}
