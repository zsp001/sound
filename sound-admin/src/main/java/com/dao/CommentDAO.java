package com.dao;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sound.model.CommentModel;

@Repository
public interface CommentDAO extends JpaRepository<CommentModel,Long>{

	@Query("select c from CommentModel c where c.newsId=:newsId")
	@Modifying
	List<CommentModel> findByNewsId(@Param("newsId") Long newsId);
}
