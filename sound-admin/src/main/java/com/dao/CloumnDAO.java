package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sound.model.ColumnModel;
@Repository
public interface CloumnDAO extends JpaRepository<ColumnModel,Long>{

}
