package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sound.model.NewsContentModel;

@Repository
public interface NewsContentDAO extends JpaRepository<NewsContentModel,Long>{
	
	

}
