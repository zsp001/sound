package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sound.model.UserOpertion;

//用户挺播报的记录dao
@Repository
public interface UserOpertionDAO extends JpaRepository<UserOpertion,Long> {

}
