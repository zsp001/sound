package com.dao;

import com.sound.model.SystemMsgModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/4/29.
 */
@Repository
public interface SysMsgDAO extends JpaRepository<SystemMsgModel,Long> {
}
