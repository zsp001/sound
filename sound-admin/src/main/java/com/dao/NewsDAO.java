package com.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sound.model.NewsModel;

@Repository
public interface NewsDAO extends JpaRepository<NewsModel,Long>{

    @Query("select u from news u")
    public Page<NewsModel> findByPage(Pageable pageable);
    
    @Query(value="select * from comment u where u.news_id=:newsId order by u.create_time desc limit :page,:size", nativeQuery = true)
	@Modifying
	public Object [] findByPageAndNewsId(@Param("page") Integer page,@Param("size") Integer size,@Param("newsId") Integer newsId);
}
