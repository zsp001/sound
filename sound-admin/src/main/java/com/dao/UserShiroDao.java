package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.sound.model.UserShiro;

@Repository
public interface UserShiroDao extends JpaRepository<UserShiro, Long>,PagingAndSortingRepository<UserShiro, Long>{

	public UserShiro findByName(String name);

}
