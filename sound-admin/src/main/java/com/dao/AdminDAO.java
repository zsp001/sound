package com.dao;

import com.sound.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/5/5.
 */
@Repository
public interface AdminDAO extends JpaRepository<Admin,Long> {
	
	
}
