package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sound.model.TypeModel;

@Repository
public interface TypeDAO extends JpaRepository<TypeModel,Long>{

	@Query("select u from type u where u.priority=:priority")
	@Modifying
	public List<TypeModel> findByPriority(@Param("priority") Integer priority);
}
