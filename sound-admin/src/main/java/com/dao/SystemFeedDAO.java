package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sound.model.UserMsgModel;

@Repository
public interface SystemFeedDAO extends JpaRepository<UserMsgModel,Long>{

}
