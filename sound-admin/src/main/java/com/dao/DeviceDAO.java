package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sound.model.Device;

@Repository
public interface DeviceDAO extends JpaRepository<Device,Long> {

	/**
	 * 根据用户id查找设备
	 * @param userId
	 * @return
	 */
	@Query("select u from device u where u.userId=:userId")
	@Modifying
	List<Device> findByUserId(@Param("userId") String userId);
}
