package com.dao;


import com.sound.model.SystemRecommend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.transaction.Transactional;

/**
 * Created by Administrator on 2018/4/26.
 */
@Repository
public interface SysRecommendDAO extends JpaRepository<SystemRecommend,Long> {

    //获取系统列表
    @Query("select u from sysRecommend u order by u.priority desc")
    public List<SystemRecommend> findByPrio();
    
    @Query("delete from sysRecommend u where u.albumId=:albumId")
    @Modifying
    public Integer deleteByAlbumId(@Param("albumId") Long albumId);
}
