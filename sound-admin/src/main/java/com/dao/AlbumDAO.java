package com.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sound.model.AlbumModel;
@Repository
public interface AlbumDAO<T> extends JpaRepository<AlbumModel, Long>{
	
	
	@Query("select u from album u where u.priority=:priority")
	@Modifying
	public List<AlbumModel> findByPriority(@Param("priority") Integer priority);
	
	@Query("update album u set u.priority=:priority where u.id=:id")
	@Transactional
	@Modifying
	public Integer updatePriority(@Param("priority") Integer priority,@Param("id") Long id);
	
	 @Query("select u from album u where u.display=true")
	 public Iterable<AlbumModel> findAlbumByDispay();
}

