package com.dao;

import com.sound.model.CollectModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/5/2.
 */
@Repository
public interface CollectDAO extends JpaRepository<CollectModel,Long> {
}
