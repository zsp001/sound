package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sound.model.TypeUserRelation;

@Repository
public interface TypeUserRelationDAO extends JpaRepository<TypeUserRelation,Long> {

}
