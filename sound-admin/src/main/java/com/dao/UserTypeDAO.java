package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sound.model.UserTypeModel;

@Repository
public interface UserTypeDAO extends JpaRepository<UserTypeModel,Long> {

	/**
	 * 根据用户id查找自定义类型
	 * @param userId
	 * @return
	 */
	@Query("select u from userType u where u.userId=:userId")
	@Modifying
	List<UserTypeModel> findByUserId(@Param("userId") String userId);

	
}
