package com.dao;

import com.sound.model.MenuModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/5/6.
 */
@Repository
public interface MenuDAO extends JpaRepository<MenuModel,Long> {
}
