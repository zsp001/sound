package com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication 
@EnableSwagger2
public class Application {
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {  
        return builder.sources(Application.class);  
    }   
    public static void main(String[] args) {
    	
    	logger.info("start spring boot .....");
		ApplicationContext ctx = SpringApplication.run(Application.class, args);
    }
}   